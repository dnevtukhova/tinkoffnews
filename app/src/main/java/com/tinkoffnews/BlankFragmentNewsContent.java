package com.tinkoffnews;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tinkoffnews.Adapter.RvAdapter;
import com.tinkoffnews.retrofit.Client;
import com.tinkoffnews.retrofit.News;
import com.tinkoffnews.retrofit.NewsContent;
import com.tinkoffnews.retrofit.Payload;
import com.tinkoffnews.retrofit.ServerApi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class BlankFragmentNewsContent extends DialogFragment {

   private TextView textViewNews;
   private View rootView;
    private String idFr;
    private static final String BUNDLE_CONTENT = "bundle_content";


    public static BlankFragmentNewsContent newInstance(final String content) {
        final BlankFragmentNewsContent blankFragmentNewsContent = new BlankFragmentNewsContent();
        final Bundle arguments = new Bundle();
        arguments.putString(BUNDLE_CONTENT, content);
        blankFragmentNewsContent.setArguments(arguments);

        return blankFragmentNewsContent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(BUNDLE_CONTENT)) {
            idFr = getArguments().getString(BUNDLE_CONTENT);
        } else {
            throw new IllegalArgumentException("Must be created through newInstance(...)");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_blank_fragment_news_content, container, false);



        textViewNews = (TextView) rootView.findViewById(R.id.news_content);
       ServerApi serverApi = Client.getInstance().getApi();

        serverApi.getNewsContent(Integer.parseInt(idFr))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<NewsContent>() {
                    @Override
                    public void onSuccess(NewsContent newsContent) {
                        Payload payload = newsContent.payload;
                        String textNews = payload.content;

                        textViewNews.setText(textNews);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

        return rootView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
