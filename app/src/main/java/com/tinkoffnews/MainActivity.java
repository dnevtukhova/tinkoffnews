package com.tinkoffnews;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.tinkoffnews.Adapter.RvAdapter;
import com.tinkoffnews.retrofit.Client;
import com.tinkoffnews.retrofit.News;
import com.tinkoffnews.retrofit.Payload;
import com.tinkoffnews.retrofit.ServerApi;

import java.util.Arrays;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    ServerApi serverApi;
    RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configureLayout();
      //  showProgress(true);
        callApiNews();
      // showProgress(false);
        onRefreshLayout(recyclerView);

    }

    private void onRefreshLayout (final RecyclerView rv)
    {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               callApiNews();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void showProgress(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }
    private void callApiNews ()
    {
        serverApi = Client.getInstance().getApi();
showProgress(true);

       serverApi.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe(new DisposableSingleObserver<News>() {
                    @Override
                    public void onSuccess(News news) {
showProgress(false);
                       displayNews(news);



                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

  //  apiCall() .map(data -> dataInMemory = data) .onErrorResumeNext(t -> data == null ? Observable.just(Data.empty()) : Observable.empty()) .startWith(readDataFromCache().map(data -> dataInMemory = data)) .subscribeOn(ioScheduler) .observeOn(uiScheduler) .subscribe(dataRequest);


    private void displayNews(News news)
    {
        Payload[] payload = news.payload;
        Arrays.sort(payload);
        RvAdapter rvAdapter = new RvAdapter(payload, getSupportFragmentManager(), recyclerView, getApplicationContext(), this);
        recyclerView.setAdapter(rvAdapter);
        rvAdapter.notifyDataSetChanged();
    }
    private void configureLayout()
    {
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rvMain);
        //прогрессБар
        progressBar = (ProgressBar)findViewById(R.id.news_progress);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
    }
}
