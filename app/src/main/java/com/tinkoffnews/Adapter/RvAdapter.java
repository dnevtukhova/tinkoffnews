package com.tinkoffnews.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//import com.tinkoffnews.BlankFragmentNewsContent;
import com.tinkoffnews.BlankFragmentNewsContent;
import com.tinkoffnews.R;
import com.tinkoffnews.retrofit.Payload;

import static com.tinkoffnews.BlankFragmentNewsContent.newInstance;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.NewsViewHolder> {
    Payload[] payloads;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    Activity activity;

    public RvAdapter(Payload[] payloads, FragmentManager fragmentManager, RecyclerView rv, Context context, Activity activity){
        this.payloads = payloads;
        this.fragmentManager=fragmentManager;
        this.recyclerView=rv;
        this.context =context;
        this.activity=activity;
    }



    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        NewsViewHolder newsViewHolder = new NewsViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = recyclerView.getChildAdapterPosition(view);
                System.out.println(pos + " это позиция!");
                Integer idNews = (payloads[pos].id);
                String id = idNews.toString();

                BlankFragmentNewsContent blankFragmentNewsContent = newInstance(id);
                fragmentManager.beginTransaction().add(blankFragmentNewsContent, "my").show(blankFragmentNewsContent).commit();//replace(R.id.news_content,blankFragmentNewsContent) .addToBackStack(null) .commit();
            }
        });

        return newsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int i) {
        newsViewHolder.newsName.setText(payloads[i].text);
    }

    @Override
    public int getItemCount() {
        return payloads.length;
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView newsName;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            newsName = (TextView) itemView.findViewById(R.id.text_news);
        }
    }
}
