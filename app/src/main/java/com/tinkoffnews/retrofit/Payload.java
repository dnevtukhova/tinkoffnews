package com.tinkoffnews.retrofit;

import java.util.Comparator;

public class Payload implements Comparable<Payload> {
    public int id;
    public String name;
    public String text;
    public PublicationDate publicationDate;
    public String bankInfoTypeId;
    public String content;

    @Override
    public int compareTo(Payload o) {
        return (int)(o.publicationDate.milliseconds-this.publicationDate.milliseconds);
    }

    public class PublicationDate
    {
        public long milliseconds;
    }

}
