package com.tinkoffnews.retrofit;

import java.util.List;
//import java.util.Observable;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServerApi {
    @GET("/v1/news")
    Single<News> getNews();

    @GET("/v1/news_content")
    Single<NewsContent> getNewsContent (
            @Query("id") Integer id
    );
}
