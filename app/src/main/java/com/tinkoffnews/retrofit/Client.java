package com.tinkoffnews.retrofit;

import android.app.Application;
import android.content.Context;

import com.tinkoffnews.BuildConfig;
import com.tinkoffnews.Cache.Cache;
import com.tinkoffnews.MainActivity;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {
    //инициализацию Retrofit’а и объекта интерфейса в классе синглтоне
    private static Client ourInstance = new Client();
    private static ServerApi api;



    private static final String BASE_URL = "https://api.tinkoff.ru";

    public static Client getInstance() {
        return ourInstance;
    }

    private Client() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel((BuildConfig.DEBUG) ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();


        api = retrofit.create(ServerApi.class);
    }

    public ServerApi getApi() {
        return api;
    }

}
